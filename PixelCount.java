/**
 * Combines two quad-trees into one resulting tree and displays the number
 * of black pixels.
 * 
 * Each string is the pre-order representation of a quadtree in which the 
 * letter 'p' indicates a parent node, the letter 'f' (full) a black 
 * quadrant and the letter 'e' (empty) a white quadrant. 
 */
public class PixelCount
{
   public static final char WHITE = 'e';  
   public static final char BLACK = 'f';
   public static final char PARENT = 'p'; 
   public static final int WHITE_VALUE = 0;
   public static final int BLACK_VALUE = 1;
   public static final int NUM_OF_LEAVES = 4; 
   public static final int NUM_OF_PIXELS = 1024; 
   
   /**
    * Combines two quad-trees and returns the number of black pixels.
    * 
    * @param quad1 is the first quad-tree
    * @param quad2 is the second quad-tree
    * @return the number of black pixels in the resulting image
    */
   public static int pixelCount(int[] quad1, int[] quad2)
   {
      int count = 0;
      
      // for every index in the tree, check if it is black
      for (int i = 0; i < NUM_OF_PIXELS; i++)
      {
         // if the value at the index in either quadtree is black,
         //    the resulting tree is also black. So count it.
         if ((quad1[i] == BLACK_VALUE) || (quad2[i] == BLACK_VALUE))
         {
            count++;
         }
      }
      return count;
   }
   
   
   /**
    * Construct a tree of values
    * @param stringTree
    * @return
    */
   public static int[] constructTree(String stringTree)
   {
      // the array that represents the tree
      int[] tree = new int[NUM_OF_PIXELS];
      // the index of 'tree'
      int treeIndex = 0;
      // the level index (0-NUM_OF_LEAVES)
      int levelIndex = 0;
      // total of 1024 pixels, 4^5 == 1024, so 5 levels of the tree
      int level = 5;
      // num of pixels that the leaf represents
      int numOfLevelLeafs = (int) Math.pow(NUM_OF_LEAVES, level);
      char currentChar;
      
      // for every character in the string representation
      for (int i = 0; i < stringTree.length(); i++)
      {
         currentChar = stringTree.charAt(i);  
         // if the character is a parent, go down a level and calculate
         //    the number of pixels at that level
         if (currentChar == PARENT)
         {
            level--;
            numOfLevelLeafs = (int) Math.pow(NUM_OF_LEAVES, level);
            levelIndex = 0;
         }
         // if the character is white, fill the pixels with white
         else if (currentChar == WHITE)
         {
            levelIndex++;
            for(int j = 0; j < numOfLevelLeafs; j++)
            {
               tree[treeIndex] = WHITE_VALUE;
               treeIndex++;
            }
         }
         // if the character is black, fill the pixels with black
         else if (currentChar == BLACK)
         {
            levelIndex++;
            for(int j = 0; j < numOfLevelLeafs; j++)
            {
               tree[treeIndex] = BLACK_VALUE;
               treeIndex++;
            }
         }
         
         // if the number of leaves has been reached, go back up the
         //    its parents level
         if (levelIndex == NUM_OF_LEAVES)
         {
            level++;
            numOfLevelLeafs = (int) Math.pow(NUM_OF_LEAVES, level);
         }
      } //for
      
      return tree;
   }
   

   /**
    * Construct two quadtrees and print the number of black pixels in the combination
    *    of the two trees.
    */
   public static void main(String[] args)
   {      
      System.out.println(pixelCount(constructTree(args[0]), constructTree(args[1])));
   }
}
